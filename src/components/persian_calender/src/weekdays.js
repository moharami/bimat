/**
 * Persian Calendar Picker Component
 *
 * Copyright 2016 Reza (github.com/rghorbani)
 * Licensed under the terms of the MIT license. See LICENSE file in the project root for terms.
 */

'use strict';

const React = require('react');
const { Text, View } = require('react-native');

const Utils = require('./utils');

function Weekdays(props) {
  const { styles, startFromMonday, weekdays, textStyle } = props;
  let wd = weekdays;
  if (!wd) {
    wd = startFromMonday ? Utils.WEEKDAYS_MON : Utils.WEEKDAYS; // English Week days Array
  }
    console.log('wwwwdddd', wd)


    return (
    <View style={styles.dayLabelsWrapper}>
      {wd.map((day, key) => {
        return (
          <Text key={key} style={[styles.dayLabels, textStyle, {color: day === 'جمعه' ? 'red' : '#FFA500', fontFamily: 'IRANSansMobile(FaNum)'}]}>
            {day}
          </Text>
        );
      })}
    </View>
  );
}

Weekdays.propTypes = {};

module.exports = Weekdays;
