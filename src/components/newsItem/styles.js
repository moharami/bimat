import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        width: '46%',
        backgroundColor: 'white',
        // position: 'relative',
        // height: 200,
        marginRight: '2%',
        marginBottom: 15
    },
    subImage: {
        width: '100%',
        height: 140,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white"

    },
});